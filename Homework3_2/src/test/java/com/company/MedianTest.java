package com.company;

import org.junit.Assert;
import org.junit.Test;

    public class MedianTest {

        @Test
        public void testIntMedian1() {
            float result = Median.median(new int[] {1, 2, 8, 64, 512});
            Assert.assertEquals(2, result, 0);
        }

        @Test
        public void testIntMedian2() {
            float result = Median.median(new int[] {10, 9, 8, 7, 6, 1});
            Assert.assertEquals(3.5, result, 0);
        }

        @Test
        public void testIntMedian3() {
            float result = Median.median(new int[] {5, 10, 15, 20});
            Assert.assertEquals(5, result, 0);
        }

        @Test
        public void testDoubleMedian1() {
            double result = Median.median(new double[] { 3, 0.1, 0.11, 0.111, 0.1, 0.11, 0.1, 0.11});
            Assert.assertEquals(0.05, result, 0);
        }

        @Test
        public void testDoubleMedian2() {
            double result = Median.median(new double[] { 0.99, 0.77, 0.55, 0.33, 0.11});
            Assert.assertEquals(0.33, result, 0);
        }

        @Test
        public void testDoubleMedian3() {
            double result = Median.median(new double[] { 0.46, 0.78, 0.34, 0.87, 0.93, 0.13});
            Assert.assertEquals(0.23, result, 0);
        }

    }
