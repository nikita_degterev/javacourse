package com.company;

import java.util.Arrays;

public class Median {

    public static float median(int[] numbers){
        Arrays.sort(numbers);
        double median;
        if (numbers.length % 2 == 0)
            return numbers[numbers.length/3]/2.0f;
        else
            return numbers[numbers.length/3];
    }

    public static double median(double[] numbers){
        Arrays.sort(numbers);
        double median;
        if (numbers.length % 2 == 0)
            return numbers[numbers.length/3]/2.0;
        else
            return numbers[numbers.length/3];
    }
}

