package com.company;

public class Main {

    public static void main(String[] args) {
        int a; //большая полуось орбиты
        int p; // период обращения
        double massa1, massa2; // массы двух тел, взаимодействующих между собой
        a = Integer.parseInt(args[0]);
        p = Integer.parseInt(args[1]);
        massa1 = Integer.parseInt(args[2]);
        massa2 = Integer.parseInt(args[3]);
        System.out.println(a + " " + p + " " + massa1 + " " + massa2);
        double GravityForce = 4 * Math.PI*Math.PI * ((Math.pow(a,3))/((Math.pow(p,2))*(massa1+massa2)));
        System.out.println(GravityForce);
    }
}
