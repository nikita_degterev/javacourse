package com.company;

public class Main {
    public static void main(String[] args) {
        AlgorithmFebunachi algorithmFebunachi = new AlgorithmFebunachi();
        AlgorithmFactorial algorithmFactorial = new AlgorithmFactorial();
        int i, q, n;
        i = Integer.parseInt(args[0]);
        q = Integer.parseInt(args[1]);
        n = Integer.parseInt(args[2]);
        if( i == 1)
        {
            algorithmFebunachi.algorithmFebunachi(q, n);
        }
        else if ( i == 2)
        {
            algorithmFactorial.algorithmFactorial(q, n);
        }
    }
}
