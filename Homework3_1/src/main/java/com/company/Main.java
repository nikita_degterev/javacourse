package com.company;

import java.math.BigDecimal;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        int i = 1;
        Scanner sc = new Scanner(System.in);
        System.out.println("Add a holder name and account balance or only a holder name: ");
        String holderName = sc.nextLine();
        BigDecimal accountBalance = sc.nextBigDecimal();
        while ( i == 1) {
            System.out.println("Choose operation:\n" +
                    "1: Get from card balance;\n" +
                    "2: Add to card balance;\n" +
                    "3: Withdraw from card balance;\n" +
                    "4: Exit.\n" +
                    "Then input sum");
            Card.operations(holderName, accountBalance);
        }
    }
}

