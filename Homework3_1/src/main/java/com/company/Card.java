package com.company;

import java.math.BigDecimal;
import java.util.Scanner;

public class Card {
    private String holderName;
    private BigDecimal accountBalance;

    public void card(String holderName, BigDecimal accountBalance) {
        this.holderName = holderName;
        this.accountBalance = accountBalance;
    }
    public void card(String holderName){
        this.holderName = holderName;
    }

    public static void operations(String holderName, BigDecimal accountBalance){
        Scanner sc = new Scanner(System.in);
        int operNumber = sc.nextInt();

        if (operNumber == 1){
            BigDecimal operSum = sc.nextBigDecimal();
            accountBalance = accountBalance.subtract(operSum);
            System.out.println("You've gotten from your card " + operSum);
            System.out.println("\nYou've added to your card " + accountBalance);
            balanceOnCard(holderName, accountBalance);

        }
        else if (operNumber == 2){
            BigDecimal operSum = sc.nextBigDecimal();
            accountBalance = accountBalance.add(operSum);
            System.out.println("You've added to your card " + operSum);
            System.out.println("\nYou've added to your card " + accountBalance);
            balanceOnCard(holderName, accountBalance);
        }
        else if (operNumber == 3){
            BigDecimal operSum = sc.nextBigDecimal();
            accountBalance = accountBalance.subtract(operSum);
            System.out.println("You've withdrawn from your card " + operSum);
            System.out.println("\nYou've added to your card " + accountBalance);
            balanceOnCard(holderName, accountBalance);
        }
        else if (operNumber == 4){ System.exit(0); }
        else {
            System.out.println("You've chosen incorrect operation.\n" +
                    "Please, choose correct number of operation,\n" +
                    "you want to operate with your card");
        }
    }


    public static void balanceOnCard(String holderName, BigDecimal accountBalance){

        int q = 0;
        BigDecimal rateE = BigDecimal.valueOf(3.6422);
        BigDecimal rateU = BigDecimal.valueOf(3.3776);
        System.out.println("Do you want to check the card balance?\n" +
                "Choose 1 to accept.\n" +
                "Choose 2 to deny.");

        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();
        if ( i == 0){
            System.out.println("What will we do next?");
        }
        else if ( i == 1){
            System.out.println("Chose exchange rate: \n" +
                    "1: Euro\n" +
                    "2: Dollars");
            q = sc.nextInt();
            if ( q == 1 ){
                accountBalance = accountBalance.multiply(rateE);
                System.out.println("Holder name: " + holderName+"\n" +
                        "Account balance: " + accountBalance);
            }
            else if ( q == 2){
                accountBalance = accountBalance.multiply(rateU);
                System.out.println("Holder name: " + holderName+"\n" +
                        "Account balance: " + accountBalance);
            }
        }
        else {
            System.out.println("You've chosen incorrect operation.\n" +
                    "Please, choose correct number of operation,\n" +
                    "you want to operate with your card");
        }
    }
}
